---
layout: default
---
# Konvivial Talks
We are a small collective aiming to connect grassroots movements with academia and everyone who is looking for an alternative way of living. To achieve this goal, we are beginning by conducting interviews with activists, open and libre innovative organizations ... and anyone with ideas on how to create a better world.

In the future we would like to conduct research motivated intrinsically, following our values to solve problems and without pressure to publish as much as possible -- research of high quality, but pursued without being driven by academic reputation or particular sources of funding. We dream of establishing an institute which is non-hierarchical, open-minded, and focused on peer-learning.

In addition to interviews and research projects, we would also like to focus on teaching and dissemination -- through workshops, as well as through an online magazine. Our concept for such a magazine involves short reviews, news, and interviews, as well as longer-form reports and analyses on topics we would like to explore in-depth.

The subject areas we are interested in are currently very broad, and we anticipate that they will continue to evolve. Our initial meetings and discussion having focused on the role of technology in society, and how alternative approaches to technology -- simple, robust and sustainable technology (we call it "convivial" or "organic" technology) -- might be useful for adaptation, resilience, as well as in daily living. 
We hope to conduct 'deep dives' into this topic, exploring contexts that include food, water, energy, housing, and digital communication -- all in an attempt to address the basic, dual-pronged question: how might technology help us construct a different society? And how is technology shaping our society now?

## Who we are
- [Don Blair](https://social.coop/@donblair)
- Valentina Treffenfeldt Montoya
- [Rafael Epplée](https://mstdn.io/@raffomania)

[Speaker Wish List](/notes/speaker%20wish%20list)