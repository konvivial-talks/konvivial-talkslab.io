### Speaker: Laura Neubauer 
**(insider: Lauri Neumann)**


**Who is Lauri?**

Lauri is an awesome friend of Vale and will help us to start our project by gaining experience in interviewing people for konvivial talks. Lauri holds a Bachelor degree in political science and is currently doing her Master degree in Diversity Study.

**What do they do:**

Lauri is an exceptional character and since I have known her she is volunteering in different projects to make Germany a better place. She worked locally with the refugees' lounge (a centre where refugees are getting help) in Hamburg Barmbek and is now an active member of a women's group in Göttingen. Additionally, she lives in a so-called house project, which holds the values of solidarity and left political activism at its firsts, it is an experiment of living together alternatively. For example they are arranging weekly sport courses for the residents or are cooking together.  

**How do they do that:**

With a lot of energy!

**Why do we want to talk to them:**

We are lucky to have Lauri around because her projects are exactly what we are looking for. Due to her activism and her experience in living in a house project, we hope that she can give us some insights on how a different convivial living is possible.
